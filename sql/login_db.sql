CREATE DATABASE  IF NOT EXISTS user_register;

USE user_register;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT, 
    usrname VARCHAR(30) NOT NULL, 
    pssword VARCHAR(30) NOT NULL, 
    usrpriv INT DEFAULT 1,
    UNIQUE(usrname),
    PRIMARY KEY(id),
    CHECK (usrpriv < 4 AND usrpriv > 0)
);