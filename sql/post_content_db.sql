USE user_register;

CREATE TABLE post_content (
    post_id INT NOT NULL AUTO_INCREMENT,
    usr_id INT NOT NULL,
    img_id VARCHAR(255) DEFAULT('placeholder.jpg'),
    headline VARCHAR(255) NOT NULL,
    adtext VARCHAR(2000) NOT NULL,
    modpost INT,
    creation_date DATE DEFAULT(current_timestamp()),
    PRIMARY KEY (post_id),
    FOREIGN KEY (usr_id) REFERENCES users(id)
);