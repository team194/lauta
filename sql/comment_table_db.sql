CREATE DATABASE  IF NOT EXISTS user_register;

USE user_register;

CREATE TABLE post_comment(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    usr_id INT NOT NULL,
    usrname VARCHAR(30),
    creation_time DATETIME NOT NULL DEFAULT(CURRENT_TIMESTAMP()),
    comment_text VARCHAR(2000),
    PRIMARY KEY(id)
)