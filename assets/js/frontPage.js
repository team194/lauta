let data;
let pageNumber
fetchJSON();
handleLogError();

//testImageLoad();
/**
 * creates a table that has five(5) rows each containing four(4) table data elements
 * 
 */
function createTable(newdata){
    let container_index = 0;
    let table = document.getElementById("contentTable");
    for(let i = 0; i < 5; i++){
        let tableRow = document.createElement("tr");
        for(let a = 0; a < 4; a++){
            let tableData = document.createElement("td");
            createTemplate(tableData, container_index,newdata);
            container_index++;
            tableRow.appendChild(tableData);
        }
        table.appendChild(tableRow);
    }
}
/**
 * Gets the content list json
 */
function fetchJSON(){
    if(!getCookie('page')){
        createCookie(1);
    }
    pageNumber = getCookie('page');
   let url = '/content_list.json';
   let xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = () =>{
       if((xhttp.status == 200 || xhttp.status == 304) && xhttp.readyState == 4){
          if(xhttp.responseText != undefined){
            data = JSON.parse(xhttp.responseText)
            //initiates table creation with content data
            //createTable(data);
            checkLength(data);
            sliceData(data,pageNumber);
          }
       }
   };
   xhttp.open('get', url,true);
   xhttp.send();
}
/**
 * Creates template for content
 * @param {Parent element} parentElement 
 * @param {index of the parent} indexNumber 
 */
function createTemplate(parentElement, indexNumber,newdata){
    if(newdata[indexNumber]){

    let mainContainer = document.createElement("div");
    mainContainer.setAttribute("id", "containerN." + indexNumber);

    let postlink = document.createElement('a');
    postlink.setAttribute("id","postlinkN." + indexNumber);
    postlink.setAttribute("href","adNmb" + newdata[indexNumber].post_id);
    
    let imageContainer = document.createElement("img");
    imageContainer.setAttribute("id", "imageN." + indexNumber);
    imageContainer.setAttribute('src', '/userimages/' + newdata[indexNumber].img_id);

    let lineBreak = document.createElement("br");

    let titleSection = document.createElement("strong");
    titleSection.setAttribute("id", "titleN." + indexNumber);
    titleSection.innerText = newdata[indexNumber].headline;

    let textSection = document.createElement("p");
    textSection.setAttribute("id", "textN." + indexNumber);
    textSection.innerText = newdata[indexNumber].adtext

    postlink.appendChild(imageContainer);
    postlink.appendChild(lineBreak);
    postlink.appendChild(titleSection);
    postlink.appendChild(textSection);
    mainContainer.appendChild(postlink);
    parentElement.appendChild(mainContainer);
    }
}

/**
 * Opening and closing "new ad" button
 */
document.getElementById("newad").onclick = function openNewAd(e) {
    document.getElementById("myForm2").style.display = "block";
    closeLogin();
    e.stopPropagation();
}

function closeNewAd() {
    document.getElementById("myForm2").style.display = "none";
}

document.getElementById("login").onclick = function openLogin(e) {
    document.getElementById("myForm").style.display = "block";
    closeNewAd();
    e.stopPropagation();
}
document.getElementById("logout").onclick = function openLogin(e) {
    document.getElementById("logoutForm").style.display = "block";
    closeNewAd();
    e.stopPropagation();
}
if(getCookie('loggedin')){
    document.getElementById('newad').setAttribute('style','display:inline-block');
}

function handleLogError(){
    let cookie = getCookie('logError');
    if(cookie == '102'){
        window.alert('incorrect username/password');
        document.cookie = "logError=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }
    else if(cookie == '103'){
        window.alert('missing username/password');
        document.cookie = "logError=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }
}

function sliceData(data,pageNumber){
    let newArr = data.slice((pageNumber-1) * 20, (pageNumber-1) * 20 + 20);
    createTable(newArr);
}
function checkLength(data){
    let arrSize = Math.floor((data.length - 1)/20);
    let numberOfPages = arrSize + 1;
    createPageNav(numberOfPages);
}
function createPageNav(numberOfPages){
    let nav = document.getElementById('pagenav');
    let ul = document.createElement('ul');
    if(pageNumber <= 5){
        for(let i = 1; i <= 5; i++){
        let li = document.createElement('li');
        let a = document.createElement('a');
        if(i != pageNumber){
        a.setAttribute('onclick','createCookie(' +i+')');
        a.setAttribute('href','/main');
        }else{
            a.setAttribute('id','crpage');
        }
        a.innerText = i;
        li.appendChild(a);
        ul.appendChild(li);
        if(i >= numberOfPages){
            break;
        }
    }
    }
    else{
        let a = Number(pageNumber);
        let firstli = document.createElement('li');
        let firsta = document.createElement('a');
        firsta.setAttribute('onclick','createCookie(1)');
        firsta.setAttribute('href','/main');
        firsta.innerText = 1;
        firstli.appendChild(firsta);
        ul.appendChild(firstli);

        firstli = document.createElement('li');
        firstli.innerText = '...';
        ul.appendChild(firstli);
        for(let i = a - 2; i <= a; i++){
            let li = document.createElement('li');
            let a = document.createElement('a');
            if(i != pageNumber){
                a.setAttribute('onclick','createCookie(' +i+')');
                a.setAttribute('href','/main');
                }else{
                    a.setAttribute('id','crpage');
                }
            a.innerText = i;
            li.appendChild(a);
            ul.appendChild(li);
}
    }
    if(pageNumber < numberOfPages && pageNumber > 4){
        let a = Number(pageNumber);
        for(let i = a + 1; i <= a + 2; i++){
                if(i > numberOfPages){
                    break;
                }
                let li = document.createElement('li');
                let a = document.createElement('a');
                a.setAttribute('onclick','createCookie(' +i+')');
                a.setAttribute('href','/main');
                a.innerText = i;
                li.appendChild(a);
                ul.appendChild(li);
    }
}
    if(numberOfPages > Number(pageNumber) + 2)
    {
    let li = document.createElement('li');
    li.innerText = "...";
    if(Number(pageNumber) + 3 != numberOfPages){
        ul.appendChild(li);
    }
    li = document.createElement('li');
    a = document.createElement('a');
    a.setAttribute('onclick','createCookie(' +numberOfPages+')');
    a.setAttribute('href','/main');
    a.innerText = numberOfPages;
    li.appendChild(a);
    ul.appendChild(li);
    }
    
    nav.appendChild(ul);
}

function createCookie(nmb){
    document.cookie = "page="+nmb+"; path=/;"
}