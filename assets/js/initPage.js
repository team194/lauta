
window.onload = init();
/**
 * Initialize the page, sets content to navigation bar dropdown menu
 * CURRENTLY USES PLACEHOLDER VALUES
 */
function init(){
    /*let ddmenu = document.getElementById("dropdownmenu");
    
    for(let i = 1; i < 5; i++){
        let a = document.createElement("a");
        a.setAttribute("href","#")
        let textNode = document.createTextNode("link " + i);
        a.appendChild(textNode);
        ddmenu.appendChild(a);
    }*/
    checkUserStatus();
}
/**
 * Function for fetching specified cookie value if exists
 * @param cookie 
 */
function getCookie(cookie){
    let name = cookie + "=";
    let cookieArray = decodeURIComponent(document.cookie).split(';');
    for(let i = 0; i < cookieArray.length; i++){
        let c = cookieArray[i];
        while(c.charAt(0) == ' '){
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function clearCookies(){
    document.cookie = "loggedin=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "usr_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "usrpriv=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}
function logout(){
    clearCookies();
    window.location.href = '/';
}
function checkUserStatus(){
    let cookie = getCookie('loggedin');
    if(cookie == 'true'){
        document.getElementById('login').setAttribute('style','display:none');
        document.getElementById('logout').setAttribute('style','display:inline-block');
        document.getElementById('logout').innerText = getCookie('username');
        
    }
    else {
        document.getElementById('login').setAttribute('style','display:inline-block');
        document.getElementById('logout').setAttribute('style','display:none');
    }
}

/**
 * Opening and closing login popup
 */
document.getElementById("login").onclick = function openLogin(e) {
    document.getElementById("myForm").style.display = "block";
    //closeNewAd();
    e.stopPropagation();
}
document.getElementById("logout").onclick = function openLogin(e) {
    document.getElementById("logoutForm").style.display = "block";
    //closeNewAd();
    e.stopPropagation();
}
 
function closeLogin() {
    document.getElementById("myForm").style.display = "none";
}
function closeLogout() {
    document.getElementById("logoutForm").style.display = "none";
}

/**
 * Getting popups out of navigations way
 */
/*document.getElementById("menubtn").onclick = function closepopups() {
    closeLogin();
    //closeNewAd();
    closeLogout();
}*/


/**
 * random kokeilu
 */

/**
document.getElementById("myForm").onmouseover = function disableCloseForm(e) {
    document.getElementById("wall").style.pointerEvents = "none";
    e.stopPropagation();
}

document.getElementById("myForm").onmouseout = function enableCloseForm(e) {
    document.getElementById("wall").style.pointerEvents = "auto";
    e.stopPropagation();
} */

//temp
function inn(){
let meta = document.getElementsByTagName('META');
if(meta[1] == undefined){
    return;
}
if(meta[1].content == getCookie('usr_id') || getCookie('usrpriv') == 3){
    document.getElementById('adDelete').setAttribute('style','display:block');
}
}
inn();