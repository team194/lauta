let data;
fetchAnswerJSON();

function fetchAnswerJSON(){
    let post_id = document.getElementsByTagName("META")[2].content;
    let url = '/comment_list' + post_id;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = () =>{
        if((xhttp.status == 200 || xhttp.status == 304) && xhttp.readyState == 4){
           if(xhttp.responseText != undefined){
             data = JSON.parse(xhttp.responseText);
             createAnswer(data);
           }
        }
    };
    xhttp.open('get', url,true);
    xhttp.send();
 }

/**
 * Creates template for content
 * @param {Parent element} parentElement 
 * @param {index of the parent} indexNumber 
 */
function createAnswer(data){
    data.forEach(answer => {
        let ele = answer.creation_time;
        let ct = ele.substring(0,10) + " " + ele.substring(11,16);
        let mainContainer = document.getElementById("answers");

        let answerContainer = document.createElement("div");
        answerContainer.setAttribute('class', 'answer');

        let answerInfo = document.createElement("div");
        answerInfo.setAttribute('class', 'userinfo');
        answerInfo.innerText = answer.usrname + " " + ct;

        let answerText = document.createElement("div");
        answerText.setAttribute('class', 'useranswer');
        answerText.innerText = answer.comment_text;

        answerContainer.appendChild(answerInfo);
        answerContainer.appendChild(answerText);
        mainContainer.appendChild(answerContainer);
    });    
    }

function answerButton(){
    
}
function addComment(){
    if(getCookie('loggedin') == 'true'){
        return true;
    }
    else{
        window.alert('You must be logged in to answer');
        return false;
    }
}