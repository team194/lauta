# Ilmoituslauta - projekti

# GET
##  "/"
- palauttaa etusivun
## "/main"
- palauttaa pääsivun
## "/content_list.json"
- hakee ilmoitukset listaan json- muodossa
## "/comment_list*"
- hakee kommentit tiettyä ilmoitusta varten json muodossa
## "/registration"
- palauttaa käyttäjän luontisivun
## "/adNmb*"
- palauttaa halutun ilmoituksen

# POST
## "/auth"
- lähettää kirjautumistiedot serverille joka käisttelee ne
## "/newad"
- lähettää uuden ilmoituksen tiedot
## "/deleteAd"
- poistaa valitun ilmoituksen
## "/adcomment"
- lähettää uuden kommentin tiedot
## "/newuser"
- lähettää uuden käyttäjän tiedot