const mysql         = require('mysql');
const express       = require('express');
const multer        = require('multer');
const bodyParser    = require('body-parser');
const path          = require('path');
const cookieParser  = require('cookie-parser');
const util          = require('util');
const fs            = require('fs');

let connection = mysql.createConnection({
    host        : 'localhost',
    user        : 'root',
    password    : 'ChoosePassword',
    database    : 'user_register'
});
let queryDB = util.promisify(connection.query).bind(connection);
let app = express();
app.use(express.static(__dirname + "/assets" ));
/*app.use(session({
    secret              : 'bazinga',
    resave              : true,
    saveUninitialized   : true
}));*/
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(cookieParser());

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + '/assets/userimages/');
    },
    filename: (req, file, cb)=>{
        cb(null, Date.now() + file.originalname);
    }
});
let mul = multer({
    storage : storage,
    fileFilter: (req,file,cb) =>{
        if(file.mimetype == "image/png" || file.mimetype == "image/jpeg" || file.mimetype == "image/gif"){
            cb(null,true);
        } else {
            return  cb(null,false);
        }
    },
    limits  : {fileSize : 10000000}
});

/**
 * Landing page
 */
app.get('/', (req,res) =>{
    res.sendFile(path.join(__dirname + '/assets/html/firstpage.html'));
});
/**
 * Main page
 */
app.get('/main',(req,res) =>{
    res.sendFile(path.join(__dirname + '/assets/html/frontpage.html'));
});

/**
 * User authentication
 */
app.post('/auth', (req,res) =>{
    let username = req.body.username;
    let password = req.body.password;
    if(username && password) {
        connection.query(
            'SELECT * FROM users WHERE usrname=? AND pssword=?',
            [username,password],
            (err,results,fields) =>{
            if(err) console.error(err);

            if(results.length > 0) {
                res.cookie('loggedin', true);
                res.cookie('username',username);
            let user_id = results[0].id;
            let usrpriv = results[0].usrpriv;
                res.cookie('usrpriv',usrpriv);
                res.cookie('usr_id',user_id);
                res.redirect('/main');
            } else {
                //logError 102 == incorrect username/password
                res.cookie('logError','102',{expires: new Date(Date.now() + 2000)});
                res.redirect('/main');
            }
            res.end();
        });
    } else {
        //logError 103 == no username / password
        res.cookie('logError','103',{expires: new Date(Date.now() + 2000)});
        res.redirect('/main');
    }
});
/**
 * Create a new ad
 */
app.post('/newad',mul.single('image'),(req,res) =>{
    let user_id =   req.cookies.usr_id;
    let headline = req.body.headline;
    let adtext = req.body.adtext;
    if(adtext.length > 2000){
        adtext = adtext.substring(0,2000);
    }
    if(headline.length > 255){
        headline = headline.substring(0,255);
    }
    let image;
    if(!user_id){
        res.sendStatus(401);
    }
    if(req.file){
        image = req.file.filename;
        if(headline){
            queryDB('INSERT INTO post_content (usr_id,img_id,headline,adtext) VALUES (?,?,?,?);',
            [user_id,image,headline,adtext]);
    }
    res.redirect('/main')
} else {
    if(headline){
        queryDB('INSERT INTO post_content (usr_id,img_id,headline,adtext) VALUES (?,?,?,?);',
        [user_id,'placeholder.jpg',headline,adtext]);
    }
    res.redirect('/main');
}
});

/**
 * Get a list of existing ads
 */
app.get('/content_list.json', async (req,res) =>{
    let json = await getEveryPost();
    res.send(json);
});

/**
 * Get a list of comments in an ad
 */
app.get('/comment_list*', async (req,res) =>{
    let post_id = req.url.substring(13,req.url.length);
    let json = await getEveryComment(post_id);
    res.send(json);
});

/**
 * Delete ad
 */
app.post('/deleteAd*',async (req,res) =>{
    let post_id = req.url.substring(9,req.url.length);
    let usr_id = req.cookies.usr_id;

    let ad = await checkPost(usr_id,post_id);
    let priv = await checkUsrPriv(usr_id);
    if(ad == true || priv[0].usrpriv == 3){
    deletePost(usr_id,post_id,priv[0].usrpriv);
    res.redirect('/main');
    }
    else{
        res.sendStatus(401);
    }
});
/**
 * Create a new comment
 */
app.post('/adcomment*', async (req,res) =>{
    let post_id = req.url.substring(10,req.url.length);
    let usr_id = req.cookies.usr_id;
    let answerText = req.body.useranswer;
    let loggedin = req.cookies.loggedin;
    if(answerText.length > 2000){
        answerText = answerText.substring(0,2000);
    }
    if(answerText && post_id && loggedin == 'true'){
       await saveComment(post_id,usr_id,answerText);
        res.redirect('/adNmb' + post_id);
    } else {
        res.sendStatus(403);
    }


});
app.get('/registration',(req,res) =>{
    res.sendFile(path.join(__dirname + '/assets/html/user_registration.html'));
});

app.post('/newuser',async (req,res) =>{
    let usrname = req.body.username;
    let pssword = req.body.password;
    let temp = await newUser(usrname,pssword);
    if(temp == false){
        res.cookie('regError','101',{expires: new Date(Date.now() + 2000)});
        res.redirect('/registration');
    }
    else{
        res.redirect('/main');
    }
});
/**
 * Open ad in full view
 */
app.get('/adNmb*', async (req,res)=>{
let post_id = req.url.substring(6,req.url.length);
let usr_id = await getPostCreator(post_id);
let ad_content = await getPost(post_id);
let temp = ad_content[0].creation_date;
let ele = String(temp);
let ct =  ele.substring(0,10) + " " + ele.substring(11,16);
res.send(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="creator_id" content="`+ usr_id[0].usr_id + `">
<meta name="post_id" content="`+ post_id +`">
<title>Ilmoituslauta</title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<link href="../css/frontpage.css" rel="stylesheet" type="text/css">
<link href="../css/openad.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="titlebar">
<h1 id="title">Ilmoituslauta</h1>
<nav class="navigation">
<div id="login" class="login">Login</div>
<div id="logout" class="login">User</div>
<div class="form-popup" id="myForm" style="display: none;">
<form class="form-container" action="auth" method="POST">
<h1>Login</h1>
<label for="username"><b>Username</b></label>
<input type="text" placeholder="Enter Username" name="username" required>
<label for="password"><b>Password</b></label>
<input type="password" placeholder="Enter Password" name="password" required>
<button type="submit" class="btn">Login</button>
<button type="button" class="btn cancel" onclick="closeLogin()">Close</button>
</form>
</div>
<div class="form-popup" id="logoutForm">
<div class="form-container">
<h1></h1>
<button type="button" class="btn" onclick="logout()">Logout</button>
<button type="button" class="btn cancel" onclick="closeLogout()">Close</button>
</div>
</div>
<div class="menubtn" id="tothefrontpage">
<a href="/main">Pääsivulle</a>
</div>
</nav>
</div>
<hr>
<div class="wall" id="wall">
<form id="adDelete" style="display:none" action="/deleteAd`+ post_id +`" method="POST">
<button type="submit">Delete ad</button>
</form>
<div id="openad">
<div id="imgcontainer">
<img id="openadimg" src="/userimages/`+ ad_content[0].img_id +`">
</div>
<div id="textcontainer">
<strong id="openadheadline"> `+ct + " -- " + ad_content[0].headline  +`</strong>        
<div id="openadtext">` + ad_content[0].adtext +`</div>
</div>
</div>
<div id="answers">
</div>
<form id="answerForm" action="adcomment`+ post_id +`" onsubmit="return addComment()" method="POST">
<textarea id="commenttextarea" cols=100 rows=20 placeholder="Enter your answer" name="useranswer" required="" maxlength="2000"></textarea>
<button type="submit">Submit</button>
</form>
</div>
</body>
<script src="../js/initPage.js" type="text/javascript">
</script>
<script src="../js/openad.js" type="text/javascript">
</script>
</html>`)
});
app.listen(8081);

/**
 * 
 * @param {number|string} usr_id ID of the current user 
 * @param {number|string} post_id ID of the post
 * @param {number|string} usrpriv User priviledge 
 */
async function deletePost(usr_id, post_id, usrpriv){
    let img_id = await queryDB('select img_id from post_content where post_id=?',[post_id]);
    if(usrpriv == 3){
        connection.query('delete from post_content where post_id=?',[post_id],(err,result) =>{
            if(err) return console.error(err);
            console.log('Post '+ post_id +' deleted succesfully via admin priv');
        });
    } else {
        connection.query('delete from post_content where usr_id=? and post_id=?',[usr_id,post_id],(err,result)=>{
            if(err) return console.error(err);
            console.log('Post '+ post_id +' deleted succesfully via user priv');
        });
    }
    deleteAsset(img_id[0].img_id);
    queryDB('delete from post_comment where post_id=?',[post_id]);
}

app.get('/debugcreate300',async (req,res)=>{
    for(let i = 0; i < 300; i++){
        await queryDB('INSERT INTO post_content (usr_id,img_id,headline,adtext) VALUES (?,?,?,?);',
        [1,'placeholder.jpg','Title','Adtext']);
    }
    res.redirect('/main');
});
/**
 * Queries the database and returns usrpriv of the given usr_id
 * @param {number|string} usr_id ID of the user
 */
async function checkUsrPriv(usr_id){
    let temp = await queryDB('select usrpriv from users where id=?',[usr_id]);
    return temp;
}

/**
 * Checks if the given post_id is linked to usr_id
 * @param {number|string} usr_id ID of the user
 * @param {number|string} post_id ID of the original post
 */
async function checkPost(usr_id,post_id){
    let temp = await queryDB('select * from post_content where post_id=? and usr_id=?',[post_id,usr_id]);
    if(temp.length>0){
        return true;
    }else{
        return false
    }
}

/**
 * Returns all posts in a json format
 */
async function getEveryPost(){
    let temp = await queryDB('select * from post_content');
    return temp;
}

/**
 * Queries the database and returns usr_id of the original post author
 * @param {number|string} post_id ID of the original post
 */
async function getPostCreator(post_id){
    let temp = await queryDB('select usr_id from post_content where post_id=?',[post_id]);
    return temp;
}

/**
 * Deletes given asset if its not 'placeholder.jpg'
 * @param {string} asset_name Name of the asset to be deleted
 */
function deleteAsset(asset_name){
    if(asset_name == "placeholder.jpg"){
        return;
    }else{
        fs.unlinkSync(path.join(__dirname + "/assets/userimages/" + asset_name));
    }
}

/**
 * Queries the database for post referred by post_id
 * @param {number|string} post_id ID of the post
 */
async function getPost(post_id){
    let temp = await queryDB('select img_id, headline, adtext, creation_date from post_content where post_id=?',[post_id]);
    return temp;
}

/**
 * Queries the database and returns all comments referring to given post_id
 * @param {number|string} post_id ID of the original post 
 */
async function getEveryComment(post_id){
    let temp = await queryDB('select * from post_comment where post_id=?',[post_id]);
    return temp;
}

/**
 * Saves the given comment to a database
 * @param {number|string} post_id ID of the original post
 * @param {number|string} usr_id User ID of the commentator
 * @param {string} commentText The comment in string format
 */
async function saveComment(post_id,usr_id,commentText){
    let temp = await queryDB('select usrname from users where id=?',[usr_id]);
    let usrname = temp[0].usrname;
    await queryDB('insert into post_comment (post_id,usr_id,usrname,comment_text) values (?,?,?,?)',[post_id,usr_id,usrname,commentText]);
}

/**
 * Creates a new user to the database and checks if name is available
 * @param {string} usrname Username
 * @param {string} pssword Password
 */
async function newUser(usrname,pssword){
   let temp = await queryDB('select usrname from users where usrname=?',[usrname]);
    if(temp.length > 0){
        return false;
    }
    else{
        await queryDB('insert into users (usrname,pssword) value (?,?)',[usrname,pssword]);
        return true;
    }   
}



/**
 * <div id="newad">Create new ad</div>
<div class="form-popup2" id="myForm2">
<form class="form-container2" action="newad" method="POST">
<h1>Create a new ad</h1>
<label for="Image" style="font-weight: bold;">Image</label><br>
<input type="file" id="myFile" name="filename"> <br>
<label for="headline"><b>Headline:</b></label>
<input type="text" placeholder="Enter headline" name="headline" required>
<label for="ad"><b>Ad text:</b></label>
<input type="textarea" placeholder="Enter text for your ad" name="adtext" required>
<button type="submit" class="btn">Post ad</button>
<button type="submit" class="btn cancel" onclick="closeNewAd()">Close</button>
</form>
</div>
 */

 /**
  * <div class="dropdown">
<div class="menubtn" id="menubtn">Navigation</div>
<div id="dropdownmenu">
</div>
</div>
  */